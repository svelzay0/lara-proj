<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// interface BarInterface {}
// class Bar implements BarInterface {}
// class SecondBar implements BarInterface {}
// App::bind('BarInterface', 'Bar');
// Route::get('bar', function (BarInterface $bar) {
//     dd($bar);
// });


Route::resource('articles', '\App\Http\Controllers\ArticleController');
Route::get('tags/{tag}','\App\Http\Controllers\ArticleController@tags');
Route::match(['get','post'],'/', '\App\Http\Controllers\ArticleController@latest')->name('welcome');
Route::middleware(['auth:sanctum', 'verified'])->get('/manager', function () {
    return 'This page only for managers';
});
