<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Tag extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    // public function articles()
    // {
    //     return $this->belongsToMany('App\Model\Article')->withTimestamps();
    // }
}
