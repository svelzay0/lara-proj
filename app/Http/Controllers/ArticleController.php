<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tag;
use App\Models\Article;
use App\Models\User;
use GuzzleHttp\Psr7\Header;
use App\Http\Requests\ArticleRequest;

class ArticleController extends Controller
{
    public function __construct(Article $article, Tag $tag) 
    {   
        $this->middleware('auth', ['only'=>['create','edit','destroy']]);
        $this->ArticleModel=$article;
        $this->TagModel=$tag;
    }
    public function index()
    {   
        $article = Article::latest('created_at')->get();
        $user = User::get();
        return view('articles.index', compact('article','user'));
    }
    public function create()
    {   
        $tag = Tag::pluck('name','id');
        return view('articles.create', compact('tag'));
    }
    public function store(ArticleRequest $request)
    {   
        $this->createArticle($request);
        return redirect('articles')->with('status', 'Статья ' .$_POST['title']. ' успешно создана.');
    }
    public function show(Article $article)
    {   
        $user = User::get();
        return view('articles.show', compact('article','user'));
    }
    public function edit($id)
    {   
        $article = Article::query();
        if (auth()->user()->id != 2) {
            $article->where('user_id', auth()->user()->id);
        }
        $article = Article::findOrFail($id);
        $tag = $this->TagModel->pluck('name','id');
        return view('articles.edit', compact('article','tag'));
    }
    public function update(Article $article, ArticleRequest $request)
    {    
        $article->update($request->all());
        $this->syncTags($article, $request->input('tag_list'));
        return redirect('articles')->with('status', 'Статья ' .$article->title. ' успешно изменена.');
    }
    private function syncTags (Article $article, array $tag) 
    {
        $article->tags()->sync($tag);
    }
    public function destroy($id)
    {   
        $article = Article::findOrFail($id);
        $article->delete($id);
        return redirect('articles')->with('status', 'Статья ' .$article->title. ' успешно удалена.');
    }
    public function createArticle (ArticleRequest $request)
    {   
        $requestAll = $request->all();
        $requestAll['user_id'] = auth()->user()->id;
        $article = Article::create($requestAll);
        $this->syncTags($article, $request->input('tag_list'));
        return $article;
    }
    public function latest() 
    {
        $latest = Article::latest()->first(); // объект в ед. числе
        $tag = Tag::pluck('name','id');
        $user = User::get();
        return view('welcome', compact('latest','tag','user'));
    }
    public function tags() 
    {
        $article = Article::latest('created_at')->get(); //коллекции в множ. числе.
        $tag = Tag::pluck('name','id');
        $user = User::get();
        return view('articles.tags', compact('article','user','tag'));
    }

}
    