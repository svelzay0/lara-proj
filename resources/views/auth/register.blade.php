<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
                        <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUTEhIVFRIXEhsWFxgXFhgYFxUWFxUXFxcVFhUYHSggGBolGxYYITEhJSkrLi4uGh8zODMtNygtLisBCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAKcBLgMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABwECBQYIBAP/xABNEAABAwIBBQoJCQUHBQEAAAABAAIDBBEFBgcSITETIkFRVGGBkZPSCBQycXKCkqHRFzNDU2JzorGyFSNVY4MWNEJSwdPwRGR0o+Il/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AJxREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQERY7F8dpaUXqaiKEHZpvDSfMCbnoQZFFotTncwhmyoc/0IZSOg6Nl8G55MJ+smHnhf8A6BBIKLQRnhwn66TsJe6rhnfwjlDx/Qm7iDfEWByaywoq/SFLMHuYAXNLXMcAdQOi8A251jMpc5mH0Uxgme90rQC5scZdo3AIDjqF7EG3Og3FFHHy1YX/ANx2P/0s5kxnDw+ul3GCV27FpcGPY5hIG2xIsSBrsCg2tERAREQEWmYrnSwuCV0T6guexxa4MjkeGuBsRpBtiQRbUV4xniwn62XsJPgg39Fg8mcraOvDjSTCQstpgtc1zb3sS1wBsbHWs4gIsBVZa4bG90b66na9ps4GVtwRtB17V8xl5hf8Qpu2Z8UGxoteGXWF/wAQpe2Z8VUZcYZ/EKXt4/ig2BFgRlrhvL6Xt4/irhllh3L6Xt4+8gziLFUWUlFK8RxVcEj3bGslY5xsLmzQbnUsm94AJJAA1knUB5yguRYz+0VHyun7aPvL00eJQy/NTRyW26D2ut7JQepERAREQEREBERAXwrqyOGN0sr2sjY0uc5xsGgcJK+6gXPtlQ6WoFBG60UID5bHy5XDSa08Ya0g+d3MEHzy4zwTzl0VDeCG9t1+mkH2fqh+LzbFGVS97zpvc57ztc4lziedxNyrbc6qwOcQ1oLnE2aALkk6gABrJQfMIQpPwDMrWysD6iZlPfXoaJkkHpWIAPNcrPNzDx8Ne/sW99BCnWqFTZ8g7OXv7FvfQ5h4+Xv7FveQa14Po/8A05P/AAn37WBW5+cHMWJCe28qIgb/AG4rMcPZ0D0qVM32bmHDHySNmdNLI0M0nNDQ1gNyA0E7Ta5J4AvHnxwTxjDXSNG/pniYehYtkHsnS9VBzmGrIZM4r4pWQVI2RTNc63Cw72QdLC5eAFHhB2XG8OAINwRcHjB2FXLTc0WM+M4XASbviG4P13N4tTSfOzRPStyQFicrMYFJRz1Lvo4iRzvOpg6XEDpWWUS+EJjGjTQUgOuaQyO9CK1h0vc0+qUEFAk6ybkm5PCSTcnrRyusqaLnENaLuJDQONxNgOtBN/g74QWwVFU4fOyCNnO2K+kR6zyPVW95xMb8Tw6omBs/c9CP7yTeN2cRN+he7JTBxSUcFMPo4mtPO+13u6XEnpUV+EXi5HitKNTTpTu4iRvGDn2vPUghmJgtbmX00V8xMOMXVRKOMdaC5wCtDVTd28YTdm8Y60FwYhaqGYcYVTOOMIN9zHYJu+JiUjeU0ZkJ+267Ix73H1VuXhGVTxBSxhxDHyvL2g6naDW6OlxgE3sstmIwTccPM7m2fUyF+sa9zbvY+g75w9JYXwjfm6L7yX9LEEJNjHEF6MNqpIJY5YXGORrwWubqO0atW0HhHCrbKrDvmjiePzQdkhECICIiAiIgIiIC5KywlLsQrXO2+NyjXxNkc0e4BdarlbOPhzoMUq2G9nTGVvO2X95fzXcR0INdK3TMw6IYtDutr7nIIr8Eujq28Ojp251pYCodRuCQQbgg2II1ggjYboOzEXOWT+eHEacBsuhVMAt+83smrUP3jdvnc0nnW84Xnxo32FRBPCTtI0ZWDpaQ78KCVEWAwLLTD6vVT1Ubnn/AToP4f8D7OOziWfQF8qqnbIx0bxdj2lrhxtcLEdRX1RBx7jGGupqianftildH5w0713SLHpXlspNz/wCDblWRVTRvZ49Fx/mRWGvnLC32VGbdmxBKfg94zoVFRSOOqVglYPtx6ngedrgfVU8LkbJHFvFK6nqb2EczdP7t28kv6riuuGm+sbEFVzDncxnxnFJrG8cNoGWOreeX+NzuoLozKXFRS0k9Q7ZFC5/nIG9b0mw6VyHpucS55u9ztJxO0ucbuPWUFx4luGaHBvGcUiuLxw3ndq1bywYD67mnoK04qevB/wAE3OjkqnCzqiSzSfqortFvO8v6gglNWSRNd5TQfOAfzV6IPh4nH9Wz2R8FQ0UX1bPZb8F6FqOOZycNpZnQTTndW20g2N7w0nXolzQRfmQbKcPh+qj9hvwVDhsH1MfsN+C04Z3cI+vf2E3dVflcwjlD+wm7iDbjhNP9RF2bfgqfsin5PD2bPgtTGdvB+Uu7CbuLYMmcqKSva99LLujWO0XXY9hBIuNTwDs4UGYAUO+EZ83RfeS/pYpjUN+Eb5FD95L+liCGWf8APejfKb6Q/MK0K4DfN9Ifmg7KCKgVUBERAREQEREBRrnjyFdWxtqaZt6qJti0bZYtZ0R9tpJI47kcSkpEHGZ1GxBBBsQRYgjUQRwFVIXTeWGbmiryXvaYp/rY7Bx9Np1P6dfOorxvMxXxa6d8dS3iB3N/suOj+JBGxaqEcCymJ5O1tOf39JPGBtJjcW7P84Bb71jGuHGgscy/At0yTzmV9CQ0vNRBfXHK4kgfYkO+b5jccy08K17UHW2SuUkFfTtnp3HROpzTqdG8bWPHAfzFiFmFy7mryhfR4jDviIZniGVvAQ82Y7ztcQb8V+NdRINHzx4H41hkpa28kFp2WGveeWOlhd7lzVGbhdlSxhzS1wu0ggg8IIsQuRso8INJVz0x+ilIbzsO+jPSwtQY17ONdRZq8Z8awyneTd7Gbi/j0ojoXPOWhrulcvAXUu+DzjGjLU0bjqc0Ts87bRya/MY+ooM34QWM7nRxUrTvp5dJ33cVnH8Zj6ioGaFu+efGfGcUkaDvKdogb6Q30h9p1vVWkAIL4qd8j2RM1ve9rGD7TyGj3lde4FhjKanhp2eTFE1g59EWJ85OvpXPeZPBfGMTbI4bymYZT94d5GOsl3qrpJAREQeTFq9lPBLPJqZFG6R3ma0n/Rch1M75ZHyyG8kjzI/0nuLne8rpPO5SVc2Huho4nSPkka2QNIDhELudYHbctaCOIlQb8n+K8hl/D3kGuhioWdOpbGMgsVH/AEM3U3vK3+wuK8gm6h8UGtOb/wA6F0lmXwTxbDY3kWkqCZ3cdnaox7Ab1lQ9hGbXEqiZkclM+GMu3732aGsvviON1tgHDbzrpiCFrGtY0Wa1oa0cQAsB1IPooc8Iwbyi+8l/SxTGoc8Is7yi+8l/SxBDAb+SDyh6Y/MKuvYqA74ekPzQdkhVVGqqAiIgIiICIiAiIgIiIC17HsiaCrB3amj0iDv2AMkF+EPbrPTcLYV8K2sjhYZJXtjY0XLnuDQOkoOXcvclHYbVbgXacb26cTzYEtuRZw/zAjXx6itcK3POvlRHiFaHQa4YY9zY6xGmdIuc8A7BewHmvwrTAdSCsLiHscLkh7SBxkOFgOpdlNOpclZHYa6qxCmgb/inaXczGHTefZaV1sgKCPCDwXQqIKxo1Ss3J/ps1tJ87SR6indahnXwQ1eGTtaLyRgTR8elHviBzlukOlBzEVlslcZdRVcVUzWWaVxfU4OaW2PWD0LEsk/JVLfegrK9z3Oe86T3uLnHhLnEuJPnJXzdqX0AX1w+hfUTRQM8uWRsY5tIgX6Br6EE+ZiMD3DD93cP3lS8v/ptu2MeY2c71lJK8+H0bIYo4mCzI2NY0cTWgAe4LUMuc5dNh0rYHRSSylgeQzRAY0khuk5x2mx1Di5wg3dFEjc+1NyOe3pR/FXjPnTcjqPai7yCWEUUDPnS8jqeuLvp8ulJySp/9XfQSuiitufKi5LVdUP+4hz50XJarqh/3EEqIvBgOKsqqeKpjDgyVge0PFnAHgIBI6ivegKHfCL8ii+8l/SxTEoe8InyKL7yX9LEEMNHxVD5TfSH5o1XNF3N9Ifmg7GCqqAqqAiIgIiIKFBdVRAXJOL47VS1Ez31ExLpXHVI8Ab42DQDYACwsutlyljeStdDUTMdSzOtK6zmRPc1wLiWua5oIIIsUGLbidRymcf1pO8qjFqrlVR20neV5wmpB10s4/oyd1P2XU8ln7GTuoKNxmrt/eqnt5e8vNV1EkhvLI+Qjhe9zyD6xK9H7HquS1HYyd1emlyarpDZlHUOOr6F4GvnIsEGKTbYNBLibAAXJJNgAOEkrfsGzRYlPYytZTM2EyO0nW4wxl79JClvIrNxR4eA8N3ap4ZnjWD/AC27GD386DE5oMgjQxmoqWjxuVttHUdxj26F/wDMdrrcQHApIREBUc24sdhVUQclZY4N4nX1FOBZrJSWavo379nU1wHQsU4camzPZkTUVMsVXSRGVwj3OVrfKsHXY4N4fKcDbXsUXOyOxLhoansnfBBg9ikbMPgm7V76hwuynj1XH0kl2tt5mh/WFqn9jcSOoUNTc/ynD3kWCnzNNkw+hoQ2ZujPLIZZBcHRvZrWkjVqa0bOElBuZNtZ2LkrK/F/HK6oqdei+UhnD+7ZZjPwtB6V07llHUOoahtK3SqHQubGLhutw0SQTqBAJI5wFzw3Nni41eJO7SLvoNTaxX6GrZwra/k3xfkT/bi76tGbnFrf3J/tx99BqwZtVQxbP8nWLcikt6UfeVTm9xbkMvtR95Bq7mDqX2w3Dn1E8VOzypZGxjm0jYu6Bc9C2F2b7FeQy9bO8t2zSZA1UVb41WQGJsTCIw4tu6RwDdIAE2AaXdJCCZaKlbFGyJgsxjAxo4mtAAHUF9kRAUO+EV83RfeS/pYpiUZZ9Mnqiqp4H08bpTDI4vY0XfovaBpNaNZsW7BxoIEb+SaPWsgMBrLf3Oo7GTuqhwSs5JU9hJ3UHhEr7jfv9t3xVRPJb5x/tu1+9ev9iVfJKjsZO6n7IqtnitR2MndQeVtTJb52T23fFUfWTN1iaUEawRI+9xwjWvU7Cank0/ZP7qsfhFUdQpprnUBuT9Z4tm1B1bk3VumpKeV/lSU8b3ek5jSfeVklj8n6R0VLTxO8qOCNjvO1gB94WQQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQf/Z" style="height:50px;width:100px;">
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <div>
                <x-jet-label for="name" value="{{ __('Name') }}" />
                <x-jet-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
            </div>

            <div class="mt-4">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
            </div>

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
            </div>

            <div class="mt-4">
                <x-jet-label for="password_confirmation" value="{{ __('Confirm Password') }}" />
                <x-jet-input id="password_confirmation" class="block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="new-password" />
            </div>

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-jet-button class="ml-4">
                    {{ __('Register') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
