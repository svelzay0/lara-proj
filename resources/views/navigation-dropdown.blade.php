<nav x-data="{ open: false }" class="bg-white border-b border-gray-100">
    <!-- Primary Navigation Menu -->
    <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div class="flex justify-between h-16">
            <div class="flex">
                <!-- Logo -->
                <div class="flex-shrink-0 flex items-center">
                    <a href="{{ route('welcome') }}">
                        <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUTEhIVFRIXEhsWFxgXFhgYFxUWFxUXFxcVFhUYHSggGBolGxYYITEhJSkrLi4uGh8zODMtNygtLisBCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAKcBLgMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABwECBQYIBAP/xABNEAABAwIBBQoJCQUHBQEAAAABAAIDBBEFBgcSITETIkFRVGGBkZPSCBQycXKCkqHRFzNDU2JzorGyFSNVY4MWNEJSwdPwRGR0o+Il/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AJxREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQERY7F8dpaUXqaiKEHZpvDSfMCbnoQZFFotTncwhmyoc/0IZSOg6Nl8G55MJ+smHnhf8A6BBIKLQRnhwn66TsJe6rhnfwjlDx/Qm7iDfEWByaywoq/SFLMHuYAXNLXMcAdQOi8A251jMpc5mH0Uxgme90rQC5scZdo3AIDjqF7EG3Og3FFHHy1YX/ANx2P/0s5kxnDw+ul3GCV27FpcGPY5hIG2xIsSBrsCg2tERAREQEWmYrnSwuCV0T6guexxa4MjkeGuBsRpBtiQRbUV4xniwn62XsJPgg39Fg8mcraOvDjSTCQstpgtc1zb3sS1wBsbHWs4gIsBVZa4bG90b66na9ps4GVtwRtB17V8xl5hf8Qpu2Z8UGxoteGXWF/wAQpe2Z8VUZcYZ/EKXt4/ig2BFgRlrhvL6Xt4/irhllh3L6Xt4+8gziLFUWUlFK8RxVcEj3bGslY5xsLmzQbnUsm94AJJAA1knUB5yguRYz+0VHyun7aPvL00eJQy/NTRyW26D2ut7JQepERAREQEREBERAXwrqyOGN0sr2sjY0uc5xsGgcJK+6gXPtlQ6WoFBG60UID5bHy5XDSa08Ya0g+d3MEHzy4zwTzl0VDeCG9t1+mkH2fqh+LzbFGVS97zpvc57ztc4lziedxNyrbc6qwOcQ1oLnE2aALkk6gABrJQfMIQpPwDMrWysD6iZlPfXoaJkkHpWIAPNcrPNzDx8Ne/sW99BCnWqFTZ8g7OXv7FvfQ5h4+Xv7FveQa14Po/8A05P/AAn37WBW5+cHMWJCe28qIgb/AG4rMcPZ0D0qVM32bmHDHySNmdNLI0M0nNDQ1gNyA0E7Ta5J4AvHnxwTxjDXSNG/pniYehYtkHsnS9VBzmGrIZM4r4pWQVI2RTNc63Cw72QdLC5eAFHhB2XG8OAINwRcHjB2FXLTc0WM+M4XASbviG4P13N4tTSfOzRPStyQFicrMYFJRz1Lvo4iRzvOpg6XEDpWWUS+EJjGjTQUgOuaQyO9CK1h0vc0+qUEFAk6ybkm5PCSTcnrRyusqaLnENaLuJDQONxNgOtBN/g74QWwVFU4fOyCNnO2K+kR6zyPVW95xMb8Tw6omBs/c9CP7yTeN2cRN+he7JTBxSUcFMPo4mtPO+13u6XEnpUV+EXi5HitKNTTpTu4iRvGDn2vPUghmJgtbmX00V8xMOMXVRKOMdaC5wCtDVTd28YTdm8Y60FwYhaqGYcYVTOOMIN9zHYJu+JiUjeU0ZkJ+267Ix73H1VuXhGVTxBSxhxDHyvL2g6naDW6OlxgE3sstmIwTccPM7m2fUyF+sa9zbvY+g75w9JYXwjfm6L7yX9LEEJNjHEF6MNqpIJY5YXGORrwWubqO0atW0HhHCrbKrDvmjiePzQdkhECICIiAiIgIiIC5KywlLsQrXO2+NyjXxNkc0e4BdarlbOPhzoMUq2G9nTGVvO2X95fzXcR0INdK3TMw6IYtDutr7nIIr8Eujq28Ojp251pYCodRuCQQbgg2II1ggjYboOzEXOWT+eHEacBsuhVMAt+83smrUP3jdvnc0nnW84Xnxo32FRBPCTtI0ZWDpaQ78KCVEWAwLLTD6vVT1Ubnn/AToP4f8D7OOziWfQF8qqnbIx0bxdj2lrhxtcLEdRX1RBx7jGGupqianftildH5w0713SLHpXlspNz/wCDblWRVTRvZ49Fx/mRWGvnLC32VGbdmxBKfg94zoVFRSOOqVglYPtx6ngedrgfVU8LkbJHFvFK6nqb2EczdP7t28kv6riuuGm+sbEFVzDncxnxnFJrG8cNoGWOreeX+NzuoLozKXFRS0k9Q7ZFC5/nIG9b0mw6VyHpucS55u9ztJxO0ucbuPWUFx4luGaHBvGcUiuLxw3ndq1bywYD67mnoK04qevB/wAE3OjkqnCzqiSzSfqortFvO8v6gglNWSRNd5TQfOAfzV6IPh4nH9Wz2R8FQ0UX1bPZb8F6FqOOZycNpZnQTTndW20g2N7w0nXolzQRfmQbKcPh+qj9hvwVDhsH1MfsN+C04Z3cI+vf2E3dVflcwjlD+wm7iDbjhNP9RF2bfgqfsin5PD2bPgtTGdvB+Uu7CbuLYMmcqKSva99LLujWO0XXY9hBIuNTwDs4UGYAUO+EZ83RfeS/pYpjUN+Eb5FD95L+liCGWf8APejfKb6Q/MK0K4DfN9Ifmg7KCKgVUBERAREQEREBRrnjyFdWxtqaZt6qJti0bZYtZ0R9tpJI47kcSkpEHGZ1GxBBBsQRYgjUQRwFVIXTeWGbmiryXvaYp/rY7Bx9Np1P6dfOorxvMxXxa6d8dS3iB3N/suOj+JBGxaqEcCymJ5O1tOf39JPGBtJjcW7P84Bb71jGuHGgscy/At0yTzmV9CQ0vNRBfXHK4kgfYkO+b5jccy08K17UHW2SuUkFfTtnp3HROpzTqdG8bWPHAfzFiFmFy7mryhfR4jDviIZniGVvAQ82Y7ztcQb8V+NdRINHzx4H41hkpa28kFp2WGveeWOlhd7lzVGbhdlSxhzS1wu0ggg8IIsQuRso8INJVz0x+ilIbzsO+jPSwtQY17ONdRZq8Z8awyneTd7Gbi/j0ojoXPOWhrulcvAXUu+DzjGjLU0bjqc0Ts87bRya/MY+ooM34QWM7nRxUrTvp5dJ33cVnH8Zj6ioGaFu+efGfGcUkaDvKdogb6Q30h9p1vVWkAIL4qd8j2RM1ve9rGD7TyGj3lde4FhjKanhp2eTFE1g59EWJ85OvpXPeZPBfGMTbI4bymYZT94d5GOsl3qrpJAREQeTFq9lPBLPJqZFG6R3ma0n/Rch1M75ZHyyG8kjzI/0nuLne8rpPO5SVc2Huho4nSPkka2QNIDhELudYHbctaCOIlQb8n+K8hl/D3kGuhioWdOpbGMgsVH/AEM3U3vK3+wuK8gm6h8UGtOb/wA6F0lmXwTxbDY3kWkqCZ3cdnaox7Ab1lQ9hGbXEqiZkclM+GMu3732aGsvviON1tgHDbzrpiCFrGtY0Wa1oa0cQAsB1IPooc8Iwbyi+8l/SxTGoc8Is7yi+8l/SxBDAb+SDyh6Y/MKuvYqA74ekPzQdkhVVGqqAiIgIiICIiAiIgIiIC17HsiaCrB3amj0iDv2AMkF+EPbrPTcLYV8K2sjhYZJXtjY0XLnuDQOkoOXcvclHYbVbgXacb26cTzYEtuRZw/zAjXx6itcK3POvlRHiFaHQa4YY9zY6xGmdIuc8A7BewHmvwrTAdSCsLiHscLkh7SBxkOFgOpdlNOpclZHYa6qxCmgb/inaXczGHTefZaV1sgKCPCDwXQqIKxo1Ss3J/ps1tJ87SR6indahnXwQ1eGTtaLyRgTR8elHviBzlukOlBzEVlslcZdRVcVUzWWaVxfU4OaW2PWD0LEsk/JVLfegrK9z3Oe86T3uLnHhLnEuJPnJXzdqX0AX1w+hfUTRQM8uWRsY5tIgX6Br6EE+ZiMD3DD93cP3lS8v/ptu2MeY2c71lJK8+H0bIYo4mCzI2NY0cTWgAe4LUMuc5dNh0rYHRSSylgeQzRAY0khuk5x2mx1Di5wg3dFEjc+1NyOe3pR/FXjPnTcjqPai7yCWEUUDPnS8jqeuLvp8ulJySp/9XfQSuiitufKi5LVdUP+4hz50XJarqh/3EEqIvBgOKsqqeKpjDgyVge0PFnAHgIBI6ivegKHfCL8ii+8l/SxTEoe8InyKL7yX9LEEMNHxVD5TfSH5o1XNF3N9Ifmg7GCqqAqqAiIgIiIKFBdVRAXJOL47VS1Ez31ExLpXHVI8Ab42DQDYACwsutlyljeStdDUTMdSzOtK6zmRPc1wLiWua5oIIIsUGLbidRymcf1pO8qjFqrlVR20neV5wmpB10s4/oyd1P2XU8ln7GTuoKNxmrt/eqnt5e8vNV1EkhvLI+Qjhe9zyD6xK9H7HquS1HYyd1emlyarpDZlHUOOr6F4GvnIsEGKTbYNBLibAAXJJNgAOEkrfsGzRYlPYytZTM2EyO0nW4wxl79JClvIrNxR4eA8N3ap4ZnjWD/AC27GD386DE5oMgjQxmoqWjxuVttHUdxj26F/wDMdrrcQHApIREBUc24sdhVUQclZY4N4nX1FOBZrJSWavo379nU1wHQsU4camzPZkTUVMsVXSRGVwj3OVrfKsHXY4N4fKcDbXsUXOyOxLhoansnfBBg9ikbMPgm7V76hwuynj1XH0kl2tt5mh/WFqn9jcSOoUNTc/ynD3kWCnzNNkw+hoQ2ZujPLIZZBcHRvZrWkjVqa0bOElBuZNtZ2LkrK/F/HK6oqdei+UhnD+7ZZjPwtB6V07llHUOoahtK3SqHQubGLhutw0SQTqBAJI5wFzw3Nni41eJO7SLvoNTaxX6GrZwra/k3xfkT/bi76tGbnFrf3J/tx99BqwZtVQxbP8nWLcikt6UfeVTm9xbkMvtR95Bq7mDqX2w3Dn1E8VOzypZGxjm0jYu6Bc9C2F2b7FeQy9bO8t2zSZA1UVb41WQGJsTCIw4tu6RwDdIAE2AaXdJCCZaKlbFGyJgsxjAxo4mtAAHUF9kRAUO+EV83RfeS/pYpiUZZ9Mnqiqp4H08bpTDI4vY0XfovaBpNaNZsW7BxoIEb+SaPWsgMBrLf3Oo7GTuqhwSs5JU9hJ3UHhEr7jfv9t3xVRPJb5x/tu1+9ev9iVfJKjsZO6n7IqtnitR2MndQeVtTJb52T23fFUfWTN1iaUEawRI+9xwjWvU7Cank0/ZP7qsfhFUdQpprnUBuT9Z4tm1B1bk3VumpKeV/lSU8b3ek5jSfeVklj8n6R0VLTxO8qOCNjvO1gB94WQQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQf/Z" style="height:50px;width:100px;">
                    </a>
                </div>

                <!-- Navigation Links -->
                <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
                    <x-jet-nav-link href="{{ route('welcome') }}" :active="request()->routeIs('welcome')">
                        {{ __('Главная') }}
                    </x-jet-nav-link>
                </div>
                <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
                    <x-jet-nav-link href="/articles" :active="request()->routeIs('articles')">
                        {{ __('Статьи') }}
                    </x-jet-nav-link>
                </div>
                <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
                    <x-jet-nav-link :active="request()->routeIs('medals')">
                        {{ __('Медали') }}
                    </x-jet-nav-link>
                </div>
            </div>
            @auth
            <!-- Settings Dropdown -->
            <div class="hidden sm:flex sm:items-center sm:ml-6">
           
            id: {{ Auth::user()->id }} - 
            {{ Auth::user()->name }} 
            
                <x-jet-dropdown align="right" width="48">
                    <x-slot name="trigger">
                        @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
                            <button class="flex text-sm border-2 border-transparent rounded-full focus:outline-none focus:border-gray-300 transition duration-150 ease-in-out">
                                <img class="h-8 w-8 rounded-full object-cover" src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->name }}" />
                            </button>
                        @else
                            <button class="flex items-center text-sm font-medium text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out">
                                <div>{{ Auth::user()->name }}</div>

                                <div class="ml-1">
                                    <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                        <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                    </svg>
                                </div>
                            </button>
                        @endif
                    </x-slot>

                    <x-slot name="content">
                        <!-- Account Management -->
                        <div class="block px-4 py-2 text-xs text-gray-400">
                            {{ __('Manage Account') }}
                        </div>

                        <x-jet-dropdown-link href="{{ route('profile.show') }}">
                            {{ __('Profile') }}
                        </x-jet-dropdown-link>

                        @if (Laravel\Jetstream\Jetstream::hasApiFeatures())
                            <x-jet-dropdown-link href="{{ route('api-tokens.index') }}">
                                {{ __('API Tokens') }}
                            </x-jet-dropdown-link>
                        @endif

                        <div class="border-t border-gray-100"></div>

                        <!-- Team Management -->
                        @if (Laravel\Jetstream\Jetstream::hasTeamFeatures())
                            <div class="block px-4 py-2 text-xs text-gray-400">
                                {{ __('Manage Team') }}
                            </div>

                            <!-- Team Settings -->
                            <x-jet-dropdown-link href="{{ route('teams.show', Auth::user()->currentTeam->id) }}">
                                {{ __('Team Settings') }}
                            </x-jet-dropdown-link>

                            @can('create', Laravel\Jetstream\Jetstream::newTeamModel())
                                <x-jet-dropdown-link href="{{ route('teams.create') }}">
                                    {{ __('Create New Team') }}
                                </x-jet-dropdown-link>
                            @endcan

                            <div class="border-t border-gray-100"></div>

                            <!-- Team Switcher -->
                            <div class="block px-4 py-2 text-xs text-gray-400">
                                {{ __('Switch Teams') }}
                            </div>

                            @foreach (Auth::user()->allTeams() as $team)
                                <x-jet-switchable-team :team="$team" />
                            @endforeach

                            <div class="border-t border-gray-100"></div>
                        @endif

                        <!-- Authentication -->
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf

                            <x-jet-dropdown-link href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                            this.closest('form').submit();">
                                {{ __('Logout') }}
                            </x-jet-dropdown-link>
                        </form>
                    </x-slot>
                </x-jet-dropdown>
            </div>

            <!-- Hamburger -->
            <div class="-mr-2 flex items-center sm:hidden">
                <button @click="open = ! open" class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out">
                    <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                        <path :class="{'hidden': open, 'inline-flex': ! open }" class="inline-flex" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                        <path :class="{'hidden': ! open, 'inline-flex': open }" class="hidden" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
            </div>
        </div>
    </div>

    <!-- Responsive Navigation Menu -->
    <div :class="{'block': open, 'hidden': ! open}" class="hidden sm:hidden">
        <div class="pt-2 pb-3 space-y-1">
            <x-jet-responsive-nav-link href="{{ route('welcome') }}" :active="request()->routeIs('/')">
            {{ __('Главная') }}
            </x-jet-responsive-nav-link>
        </div>

        <!-- Responsive Settings Options -->
        <div class="pt-4 pb-1 border-t border-gray-200">
            <div class="flex items-center px-4">
                <div class="flex-shrink-0">
                    <img class="h-10 w-10 rounded-full" src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->name }}" />
                </div>

                <div class="ml-3">
                    <div class="font-medium text-base text-gray-800">{{ Auth::user()->name }}</div>
                    <div class="font-medium text-sm text-gray-500">{{ Auth::user()->email }}</div>
                </div>
            </div>

            <div class="mt-3 space-y-1">
                <!-- Account Management -->
                <x-jet-responsive-nav-link href="{{ route('profile.show') }}" :active="request()->routeIs('profile.show')">
                    {{ __('Profile') }}
                </x-jet-responsive-nav-link>

                @if (Laravel\Jetstream\Jetstream::hasApiFeatures())
                    <x-jet-responsive-nav-link href="{{ route('api-tokens.index') }}" :active="request()->routeIs('api-tokens.index')">
                        {{ __('API Tokens') }}
                    </x-jet-responsive-nav-link>
                @endif

                <!-- Authentication -->
                <form method="POST" action="{{ route('logout') }}">
                    @csrf

                    <x-jet-responsive-nav-link href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                this.closest('form').submit();">
                        {{ __('Logout') }}
                    </x-jet-responsive-nav-link>
                </form>

                <!-- Team Management -->
                @if (Laravel\Jetstream\Jetstream::hasTeamFeatures())
                    <div class="border-t border-gray-200"></div>

                    <div class="block px-4 py-2 text-xs text-gray-400">
                        {{ __('Manage Team') }}
                    </div>

                    <!-- Team Settings -->
                    <x-jet-responsive-nav-link href="{{ route('teams.show', Auth::user()->currentTeam->id) }}" :active="request()->routeIs('teams.show')">
                        {{ __('Team Settings') }}
                    </x-jet-responsive-nav-link>

                    <x-jet-responsive-nav-link href="{{ route('teams.create') }}" :active="request()->routeIs('teams.create')">
                        {{ __('Create New Team') }}
                    </x-jet-responsive-nav-link>

                    <div class="border-t border-gray-200"></div>

                    <!-- Team Switcher -->
                    <div class="block px-4 py-2 text-xs text-gray-400">
                        {{ __('Switch Teams') }}
                    </div>

                    @foreach (Auth::user()->allTeams() as $team)
                        <x-jet-switchable-team :team="$team" component="jet-responsive-nav-link" />
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    @else
    <div class="flex items-center px-4">
                <div class="ml-3">
                    <div class="font-medium text-base text-gray-800">
                        <a href='/login'>{{ __('Войти') }}</a>
                    </div>
                    
                </div>
                <div class="ml-3">
                <div class="font-medium text-base text-gray-800">
                        <a href='/register'>{{ __('Регистрация') }}</a>
                    </div>
            </div>
</div>
    @endauth
</nav>
