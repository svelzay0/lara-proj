<x-app-layout>

  <div class="p-3">
    <div class="input-group justify-content-center">
      <h3>Редактировать: {{$article->title}}</h3>
    </div>
    <form action="{{route('articles.update',$article->id)}}" method="POST"> 
      @csrf
      @method('PATCH')
      @include('articles.form',['titleValue'=>$article->title,'textarea'=>$article->body,'submitButton'=>'Изменить'])
    </form>
    @include('errors.list')
    <div class="input-group-text justify-content-center">
      <b>Edit by: {{Auth::user()->name}}</b>
    </div>
  </div>

</x-app-layout>