<x-app-layout>

  <div class="p-3">
    
    <div class="input-group justify-content-center">
      <h3>Добавить новую статью:</h3>
    </div>
    <form action="/articles" method="POST">
      @csrf
      @method('POST')
        @include('articles.form',['titleValue'=>null,'textarea'=>null,'submitButton'=>'Добавить'])
    </form>
    @include('errors.list')
    <div class="input-group-text justify-content-center">
      <b>Author: {{Auth::user()->name}}</b>
    </div>
  </div>
  
  </div>
  
</x-app-layout>