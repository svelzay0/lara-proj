    <div class="input-group-prepend">
      <span class="input-group-text" id="basic-addon1"><b>Введите название:</b></span>
      <input type='text' class="form-control" value="{{$titleValue}}" aria-describedby="basic-addon1" name='title'>
    </div>
    
    <div class="input-group-text justify-content-center">
      <span id="basic-addon1"><b> Категории статьи: </b></span>
    </div> 
    <div>
      <select class="form-control" multiple name="tag_list[]">
        @foreach ($tag as $id => $name)
          @if ($_SERVER["REQUEST_URI"]!="/articles/create")
            @if ($article->tags->pluck('id')->contains($id))
            <option class="input-group p-2 justify-content-center" value="{{$id}}" selected>
              {{$name}}
            </option>
            @else 
            <option class="input-group p-2 justify-content-center" value="{{$id}}">
              {{$name}}
            </option> 
            @endif
          @else
            <option class="input-group p-2 justify-content-center" value="{{$id}}">
              {{$name}}
            </option> 
          @endif  
        @endforeach 
      </select>
    </div>

    <div class="input-group-prepend">
      <span class="input-group-text" id="basic-addon1"><b>Введите описание:</b></span>
      <textarea type='text' class="form-control" name='body'>{{$textarea}}</textarea>
      <button type="submit" class="btn btn-outline-secondary"><b>{{$submitButton}}</b></button>
    </div>

<script>$(document).ready(function() {
    $('.form-control').select3();
});</script>