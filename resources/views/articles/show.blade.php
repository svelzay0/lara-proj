<x-app-layout>

    <div class='p-3'>
        
        <div class="input-group justify-content-center">
            
            <article>
            <div> 
                
                <div class='p-2'>
                    <h3>{{$article->title}}</h3>
                    {{$article->created_at}} 
                    by
                    @foreach ($user as $value)
                        @if ($value->id==$article->user_id)
                            {{$value->name}}
                        @endif
                    @endforeach                  
                </div>   

                <div class="d-flex">
                @auth
                    @if (auth()->user()->id==$article->user_id || auth()->user()->id==2)
                        <div class="p-2">
                            <a class="btn btn-warning" href="/articles/{{$article->id}}/edit">Редактировать</a>
                        </div>
                        <form action="{{route('articles.destroy',$article)}}" method="POST"> 
                            <div class="p-2">
                                @csrf
                                @method('DELETE')
                                <input class="btn btn-warning" type='submit' href="/articles/{{$article->id}}" value="Удалить">
                            </div>
                        </form>
                    @endif
                @endauth
                </div>

            </div>
            
            <hr/>    
            {{$article->body}}
            <hr/>
        </article>

        </div>
        @unless ($article->tags->isEmpty())
            <h5>Категории: </h5>
            <ul>
                @foreach ($article->tags as $tag)
                <li>- {{$tag->name}}</li>
                @endforeach
            </ul>
        @endunless

    </div>
    

</x-app-layout>