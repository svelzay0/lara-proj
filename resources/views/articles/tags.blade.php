<x-app-layout>

    <div class='p-3'>
        @unless (empty($article))
        <div class="input-group justify-content-center">
            <h3>{{explode('/',$_SERVER['REQUEST_URI'])[2]}}</h3>
        </div>        
            @foreach ($article as $elem)
                @if ($elem->tags->pluck('name')->contains(explode('/',$_SERVER['REQUEST_URI'])[2]))
                    <hr/>
                    <article>
                        <div class="d-flex"> 
                            <div class="mr-auto p-2">
                            {{$elem->created_at->diffForHumans()}}
                            @if (!$elem->user_id==null) 
                                by 
                                @foreach ($user as $value)
                                    @if ($value->id==$elem->user_id)
                                        {{$value->name}}
                                    @endif
                                @endforeach
                            @endif
                                <a href="/articles/{{$elem->id}}"><h3>{{$elem->title}}</h></a>
                            </div>
                                @auth
                                    @if (auth()->user()->id==$elem->user_id || auth()->user()->id==2)
                                        <div class="p-2">
                                            <a class="btn btn-warning" href="/articles/{{$elem->id}}/edit">Редактировать</a>
                                        </div>
                                        <form action="{{route('articles.destroy',$elem)}}" method="POST"> 
                                            <div class="p-2">
                                                @csrf
                                                @method('DELETE')
                                                <input class="btn btn-warning" type='submit' href="/articles/{{$elem->id}}" value="Удалить">
                                            </div>
                                        </form>
                                    @endif
                                @endauth
                            </div>
                    </article>
                @endif  
            @endforeach        
        @else
        <div class="input-group justify-content-center">
            <h3>Статей нет.</h3>
        </div>
        @endunless   
        <hr/>
        <div class="input-group justify-content-center">
            <a class="input-group-text justify-content-center" href="/articles/create">
                <b>Создать статью</b>
            </a>
        </div>
    </div>
      
</x-app-layout>