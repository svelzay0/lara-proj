<x-app-layout>
    
    <div class="p-3 input-group justify-content-center">
        @auth
            <h3>Hello, 
                    {{ Auth::user()->name }} !
            </h3>
        @else
            <h3>
                Please log on
            </h3>    
        @endauth
    </div>
     
    <div class="p-4">    
    <div class="p-2 input-group justify-content-center">
            Категории:       
    </div>
    <hr/>
    <div class="input-group justify-content-center">
        @foreach ($tag as $name)               
        <a class="btn btn-primary" href="/tags/{{$name}}"> 
            {{$name}}
        </a>              
        @endforeach
    </div>
    <hr/>
    </div>
    @unless (empty($latest))
        <div class="p-4">
            Последняя статья: 
            <a href="/articles/{{$latest->id}}">
                <h3>
                    {{$latest->title}}
                </h3>
            </a>
            <hr/>
            {{$latest->created_at}}
            @if (!$latest->user_id==null) 
                by 
                @foreach ($user as $value)
                    @if ($value->id==$latest->user_id)
                        {{$value->name}}
                    @endif
                @endforeach
            @endif
             / 
            @unless ($latest->tags->isEmpty())
                @foreach ($latest->tags as $tag)
                    <a href="/tags/{{$tag->name}}">
                        • {{$tag->name}}
                    </a>     
                @endforeach
            @endunless
            <hr/>
            {{$latest->body}} 
        </div>
    @endunless        
</x-app-layout>
