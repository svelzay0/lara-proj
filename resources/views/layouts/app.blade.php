<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
       @include ('partials.head')
    </head>

    <body class="font-sans antialiased">
        <div class="min-h-screen bg-gray-300">

            <!-- Nav-menu -->
            @livewire('navigation-dropdown')
            
             <!-- Page Content -->
            <div class="py-12">
                <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">

                        @if (session('status'))
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session('status') }}
                            </div>                  
                        @endif

                        <main>
                            {{ $slot }} 
                        </main>

                    </div>
                </div>
            </div>

            <!-- footer -->
            <div class="input-group justify-content-center">
                <i>by svel 2020</i>
            </div>

            @stack('modals')

            @livewireScripts
            
        </div>

</body>
</html>
