@if ($errors->any())
  <hr/>
    <ul class="alert alert-danger">
      @foreach ($errors->all() as $key=>$error)
      {{$key+1}}) {{$error}}
      <br>
      @endforeach
    </ul>
  <hr/> 
  @endif